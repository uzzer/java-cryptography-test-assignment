package me.mihhailsidorin.certificate;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.security.provider.certpath.OCSP;

import java.io.*;
import java.net.URI;
import java.security.*;
import java.security.cert.*;
import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/certificates")
public class CertificateController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateController.class);

    public static final String DEFAULT_KEYSTORE_PASSWORD = "changeit";
    public static final String SIGN_WISE_TEST_JKS = "SignWiseTest.jks";

    /**
     * Show different certificate stats
     * @param request base64 encoded certificate
     * @return
     * @throws CertificateException
     */
    @RequestMapping(value = "/details", method = POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> details(@RequestBody TestRequest request) throws CertificateException{
        X509Certificate cert = getX509CertificateFromRequest(request);

        Principal principal = cert.getSubjectDN();
        String subject = principal.getName();

        principal = cert.getIssuerDN();
        String issuer = principal.getName();

        return ImmutableMap.<String, Object>builder()
                .put("subject", subject)
                .put("issuer", issuer)
                .put("notBefore", cert.getNotBefore().getTime())
                .put("notAfter", cert.getNotAfter().getTime())
                .put("serialNumber", cert.getSerialNumber())
                .put("ocspUrl", getOscpUrl(cert))
                .put("keyUsages", getKeyUsagesArray(cert))
                .build();
    }

    /**
     * Return String representation of certificate Key Usage
     *
     * @param cert X509 certificate
     * @return List of key usages
     */
    private List<String> getKeyUsagesArray(X509Certificate cert) {
        boolean[] keyUsage = cert.getKeyUsage();
        List<String> usages = new ArrayList<>();
        if (keyUsage == null) {
            return usages;
        }
        for (int i = 0; i < keyUsage.length; i++) {
            if (keyUsage[i]) {
                usages.add(KeyUsage.values()[i].toString());
            }
        }
        return usages;
    }

    private String getOscpUrl(X509Certificate cert) throws CertificateException {
        URI response = OCSP.getResponderURI(cert);
        if (response == null) {
            return "";
        }
        return response.toString();
    }

    /**
     * Get base64 encoded certificates chain from JKS
     * @param certificate
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/chain", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Collection<String> getChain(@RequestBody TestRequest certificate) throws Exception {
        X509Certificate cert = getX509CertificateFromRequest(certificate);

        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(new ClassPathResource(SIGN_WISE_TEST_JKS).getInputStream(), DEFAULT_KEYSTORE_PASSWORD.toCharArray());

        List<X509Certificate> chain = getCertificatesChain(cert, ks);

        return Collections2.transform(chain, new Function<X509Certificate, String>() {

            @Override
            public String apply(X509Certificate certificate) {
                try {
                    return new String(Base64.encodeBase64(certificate.getEncoded()));
                } catch (CertificateEncodingException e) {
                    LOGGER.error("Unable to encode certificate", e);
                    throw new RuntimeException(e);
                }
            }
        });
    }

    /**
     * Request certificate revocation status
     * @param testRequest
     * @return String certificate revocation status
     * @throws Exception
     */
    @RequestMapping (value = "/validate", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String validate(@RequestBody TestRequest testRequest) throws Exception {
        X509Certificate cert = getX509CertificateFromRequest(testRequest);

        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(new ClassPathResource(SIGN_WISE_TEST_JKS).getInputStream(), DEFAULT_KEYSTORE_PASSWORD.toCharArray());

        List<X509Certificate> chain = getCertificatesChain(cert, ks);

        X509Certificate root_cert = chain.get(chain.size() - 1);
        OCSP.RevocationStatus x = OCSP.check(cert, root_cert);

        return x.getCertStatus().toString();
    }

    private List<X509Certificate> getCertificatesChain(X509Certificate cert, KeyStore ks)
            throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException, KeyStoreException {
        List<X509Certificate> chain = new ArrayList<>();
        while (!isSelfSigned(cert)) {
            chain.add(cert);
            String issuer = cert.getIssuerDN().getName();

            Enumeration<String> alias = ks.aliases();
            Boolean parentIsFound = false;
            while (alias.hasMoreElements()) {
                X509Certificate certificateFromKeystore = (X509Certificate) ks.getCertificate(alias
                        .nextElement());
                if (certificateFromKeystore.getSubjectDN().getName().equals(issuer)) {
                    cert = certificateFromKeystore;
                    parentIsFound = true;
                }
            }
            if (!parentIsFound) {
                throw new RuntimeException("Root certificate is not found in JKS");
            }
        }
        return chain;
    }

    public static boolean isSelfSigned(X509Certificate cert)
            throws CertificateException, NoSuchAlgorithmException,
            NoSuchProviderException {
        try {
            PublicKey key = cert.getPublicKey();
            cert.verify(key);
            return true;
        } catch (SignatureException sigEx) {
            return false;
        } catch (InvalidKeyException keyEx) {
            return false;
        }
    }

    private X509Certificate getX509CertificateFromRequest(TestRequest certificate) throws CertificateException {
        byte[] decodedCertificate = Base64.decodeBase64(certificate.getCertificate());
        InputStream in = new ByteArrayInputStream(decodedCertificate);
        CertificateFactory factory = CertificateFactory.getInstance("X.509");
        return (X509Certificate) factory.generateCertificate(in);
    }

}
