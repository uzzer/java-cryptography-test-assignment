package me.mihhailsidorin.certificate;

import org.apache.commons.lang3.text.WordUtils;

public enum KeyUsage {
    DIGITAL_SIGNATURE,
    NON_REPUDIATION,
    KEY_ENCIPHERMENT,
    DATA_ENCIPHERMENT,
    KEY_AGREEMENT,
    KEY_CERTSIGN,
    CRL_SIGN,
    ENCIPHER_ONLY,
    DECIPHER_ONLY;

    public String toString() {
        return WordUtils.capitalizeFully(name().replaceAll("_", " "));
    }
}

