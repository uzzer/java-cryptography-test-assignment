package me.mihhailsidorin.error;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.text.MessageFormat;

/**
 * Controller for custom error handling
 */
@Controller
class CustomErrorController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomErrorController.class);

	/**
	 * Display an error page, as defined in web.xml <code>custom-error</code> element.
	 */
	@RequestMapping("generalError")
	@ResponseBody
	public ImmutableMap<String, Serializable> generalError(HttpServletRequest request, HttpServletResponse response) {

		// retrieve some useful information from the request
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		// String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
		String exceptionMessage = getExceptionMessage(throwable, statusCode);

		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (requestUri == null) {
			requestUri = "Unknown";
		}

		ImmutableMap<String, ? extends Serializable> responseBody = ImmutableMap.of("status_code", statusCode,
				"timestamp", LocalDateTime.now().toString(),
				"error_description", exceptionMessage,
				"stack_trace", throwable.getStackTrace());

		LOGGER.error("Error message returned", throwable);

		return ImmutableMap.of("success", "false",
				"body", responseBody);
	}

	private String getExceptionMessage(Throwable throwable, Integer statusCode) {
		if (throwable != null) {
			return Throwables.getRootCause(throwable).getMessage();
		}
		HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
		return httpStatus.getReasonPhrase();

	}
}
