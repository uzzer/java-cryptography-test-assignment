package me.mihhailsidorin.certificate;

import com.google.common.io.ByteStreams;
import me.mihhailsidorin.config.WebAppConfigurationAware;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.IsEqual.equalTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CertificateControllerTest extends WebAppConfigurationAware{

    byte[] inputFile;

    @Before
    public void setUp() throws Exception {
        inputFile = ByteStreams.toByteArray(CertificateControllerTest.class.getResourceAsStream("/input_data.json"));
    }

    @Test
    public void testDetails() throws Exception {
        mockMvc.perform(post("/certificates/details")
                            .content(inputFile)
                            .contentType(MediaType.APPLICATION_JSON))
                            .andExpect(status().isOk())
                            .andExpect(jsonPath("$.subject", equalTo("C=EE, O=SIGNWISE, OU=digital signature, SURNAME=Doe, GIVENNAME=John, SERIALNUMBER=SW123456789, CN=John Doe")))
                .andExpect(jsonPath("$.issuer").value(equalTo("C=EE, O=Cross Border Trust Services OÃ\u009C, OU=Developement, SERIALNUMBER=12243185, CN=SignWise Test Card 201403, EMAILADDRESS=pki@cbts.eu")))
                .andExpect(jsonPath("$.notBefore").value(equalTo(1398693042000L)))
                .andExpect(jsonPath("$.notAfter").value(equalTo(1461851442000L)))
                .andExpect(jsonPath("$.serialNumber").value(equalTo(1098271413675540691L)))
                .andExpect(jsonPath("$.ocspUrl").value(equalTo("http://ocsp.test.signwise.me/")))
                .andExpect(jsonPath("$.keyUsages").value(contains("Non Repudiation")))
        ;
    }

    @Test
    public void certificateChaninRequest() throws Exception {
        String certificate_one = "MIIEQTCCAymgAwIBAgIIDz3YCCcYwNMwDQYJKoZIhvcNAQEFBQAwgaExGjAYBgkqhkiG9w0BCQEWC3BraUBjYnRzLmV1MSIwIAYDVQQDDBlTaWduV2lzZSBUZXN0IENhcmQgMjAxNDAzMREwDwYDVQQFEwgxMjI0MzE4NTEVMBMGA1UECwwMRGV2ZWxvcGVtZW50MSgwJgYDVQQKDB9Dcm9zcyBCb3JkZXIgVHJ1c3QgU2VydmljZXMgT8OcMQswCQYDVQQGEwJFRTAeFw0xNDA0MjgxMzUwNDJaFw0xNjA0MjgxMzUwNDJaMIGCMREwDwYDVQQDDAhKb2huIERvZTEUMBIGA1UEBRMLU1cxMjM0NTY3ODkxDTALBgNVBCoMBEpvaG4xDDAKBgNVBAQMA0RvZTEaMBgGA1UECwwRZGlnaXRhbCBzaWduYXR1cmUxETAPBgNVBAoMCFNJR05XSVNFMQswCQYDVQQGEwJFRTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXHartKayKiJWm+QZihnt/iG1x0DeRLAj3VyEuiLeKxkfxVnZyMp71WGAPf7uxlOTuH4gYPoBQXDYQsiRaYoSfyZkOIOvngGiCjM7Ft4ez+mO3qe5PkK7FF7EticuHg6syuYyS8l8xO5JuCKEKIXGdSTMRHS/Y9+ISK0YI//V3T0F0PcfWGt+AqNS5J/Hu8dL0Zl9+xohd92Npdfb1uUtlBykqNNVqsvwnNw5szod1HHIcfYF8AHoXaGVy+obb99CIH6kxMgXwlNK9cknRSMQIs9DJDqtJwSYBuIsefliarQ3C7qNTzHG1MO7Asr+YDmkw0dpLdGy8d39tWdQ9YU6UCAwEAAaOBmTCBljA5BggrBgEFBQcBAQQtMCswKQYIKwYBBQUHMAGGHWh0dHA6Ly9vY3NwLnRlc3Quc2lnbndpc2UubWUvMB0GA1UdDgQWBBQRvbaQWM67CilJO3CEDJuHCgBLvjAJBgNVHRMEAjAAMB8GA1UdIwQYMBaAFIBSJ5FkfZ0slSTO1qmb13vURsDxMA4GA1UdDwEB/wQEAwIGQDANBgkqhkiG9w0BAQUFAAOCAQEAOjz6h4Hibk2bYHGYzm5KeC6tECCITRJRbr6xXhsShxmTBQb9cGPER/HS0maIycbdi8Bs57VuB4kpQxoNQZC8y4edO89ncxFtCDz52HiavffzUXaVkNw8rybdkl1Y+pdq8t58W99VMt9ZFXQT4DHaPXsQgYJVUajO0Ai4elhznzq9xwmmBsw7WlLVuORHM4CGqmAw8XSQYNklgFGn40Vay/Y+ODYCmuaVR2a1A2w1rNGOqGu3SlaUwIXhgH+A8wRQNHACMDa8RmmNILxV4eJcni71w1UHvntJkISNbMUwhcJMRQxRoxaTYOHG9m7FmueoQ8MjDhi8DIF5TpHAIVZV6w==";
        String certificate_two = "MIIFvDCCA6SgAwIBAgIIJEXz5qPpHVIwDQYJKoZIhvcNAQELBQAwWDEeMBwGA1UEAwwVU2lnbldpc2UgVGVzdCBSb290IENBMSkwJwYDVQQKDCBDcm9zcyBCb3JkZXJzIFRydXN0IFNlcnZpY2VzIE/DnDELMAkGA1UEBhMCRUUwHhcNMTQwMzMxMDkzMjA2WhcNMTkwMzMxMDkzMjA2WjCBoTEaMBgGCSqGSIb3DQEJARYLcGtpQGNidHMuZXUxIjAgBgNVBAMMGVNpZ25XaXNlIFRlc3QgQ2FyZCAyMDE0MDMxETAPBgNVBAUTCDEyMjQzMTg1MRUwEwYDVQQLDAxEZXZlbG9wZW1lbnQxKDAmBgNVBAoMH0Nyb3NzIEJvcmRlciBUcnVzdCBTZXJ2aWNlcyBPw5wxCzAJBgNVBAYTAkVFMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhYZIyxb474L51qHn7HmOtccF2poB3pRiMXWcnjY++JRq1hcDDBQkYtmrucqLkp6zGhkKymDtiXUxpdMvQOjJ6SfhUeKeNLtFGZkEaGWSI1AbmYFV96a+KGjM9TWsI40vmbxYbUnBC53826xaLkW6jHnS6bjg6SrWVLJkl3TgU6ztVViQMdFS0hQXRWB3aN4L4YE+Y1nmxGSZDuVnwHSJafwcSlEWoW2kQgVjMKlLHIr6X1oNLhaXUjw0OumW9FwDGyQwc+9vmfXhYndD1bbHjKN97DeBKtTX2ZMQh2jQ5z5CXIXWcvIgNwK5t1nzJAsk543KdYtzl8c7SZ1D3qc1KwIDAQABo4IBPjCCATowOQYIKwYBBQUHAQEELTArMCkGCCsGAQUFBzABhh1odHRwOi8vb2NzcC50ZXN0LnNpZ253aXNlLm1lLzAdBgNVHQ4EFgQUgFInkWR9nSyVJM7WqZvXe9RGwPEwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBRK6G8GB76hyXKLpqDXImBrecmlCTCBmwYDVR0fBIGTMIGQMIGNoIGKoIGHhoGEaHR0cDovL2NybC50ZXN0LnNpZ253aXNlLm1lLz9jbWQ9Y3JsJmlzc3Vlcj1DTj1TaWduV2lzZSUyMFRlc3QlMjBSb290JTIwQ0EsJTIwTz1Dcm9zcyUyMEJvcmRlcnMlMjBUcnVzdCUyMFNlcnZpY2VzJTIwTyVDMyU5QywlMjBDPUVFMA4GA1UdDwEB/wQEAwIBhjANBgkqhkiG9w0BAQsFAAOCAgEABTGnLJ2jLQx8Y1w5LLxzdHWv0wNBR4ZjHHkyFAtdGbPW5H4Sz5Lg1mOLSntEiVtup8IvuzhPN1HqCQ4r1KNXVEJD1vQ1BllwMPbzMcJscjJXoFT380uXHMZ8tWXrMm+1zhm3+DMO4b1FDREsvXv1ZnIcgUIsEwWFSO3MoCGghXQlwKysokhsBaRdqw2jz5IR5h4WvvNwfhu/gNAx91wbEAmOPklTYguiGfz01KyRRVWo3YJSEXIXcCGXusvQZADmyyrPCyqm5fDNat2lxTiUh5FkIh10qPWjjBb5DX6hxT4YTftRFh2+ngj5xAiKEn0ul2fAF8gtq5m/j5UzPL9Qxz9MWnkhl7bITlmO2v80GCFQYVXq+aOwcsRxEMn//fGNd12cdTtOqF+Ob3z2ZVIvjk+aM2ttNC6YcYRY2nnCeEhEEKFA3X3LLbKwe9SD/kFc9clzFjOwUFidKs7XY2JXgouFuBQFBpzeZMndNwGF8answbOOsLtYMvOtrLIK5Z/NPA9n/CVRuiG0IHaoz0Q7q6CtPnE+5FG1ZS4MBXgTOW/S9Jp+CfBQKWExSw3xPsuYjqJdfOIclpmXj2TCqmaJjtFScncxx0CjRoMfPxZzMi+pPT89r+cAwZqzCjXlz2HtEVi1wLdB9br3pVO1UaUytCoEB+2UUyKk1PfbQ+rG98Q=";
        mockMvc.perform(post("/certificates/chain")
                .content(inputFile)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", contains(certificate_one, certificate_two)));
    }

    @Test
    public void certificateValidationRequest() throws Exception {
        mockMvc.perform(post("/certificates/validate")
                .content(inputFile)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", equalTo("GOOD")));
    }

}